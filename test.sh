data_root=/asr/librispeech/data

. ./cmd.sh || exit 1
. ./path.sh || exit 1

nj=4 # number of parallel jobs
cmd=run.pl

# echo
# echo "===== ALIGN DATA ====="
# echo
# 
# # shows alighment in human readable form
# show-alignments $dir/phones.txt $dir/final.mdl ark:$dir/ali.1

dir=exp/mono_ali

for test in test_clean dev_clean; do
#	steps/align_si.sh --nj $nj --cmd "$train_cmd" \
#  	 data/$test data/lang_nosp exp/mono $dir/$test
#	$cmd JOB=1:$nj $dir/$test/log/ali-to-phones.JOB.log \
#	 ali-to-phones --per-frame=true $dir/$test/final.mdl \
#    "ark:gunzip -c $dir/$test/ali.JOB.gz|" "ark,t:|gzip -c > $dir/$test/ali-phone.JOB.gz"
#	$cmd JOB=1:$nj $dir/$test/log/lattice-to-post.JOB.log \
#	 lattice-to-post --acoustic-scale=0.1 \ 
#    "ark:gunzip -c exp/mono/decode_nosp_tgsmall_$test/lat.JOB.gz|" "ark,t:|gzip -c > $dir/$test/post.JOB.gz"
#	$cmd JOB=1:$nj $dir/$test/log/post-to-phone-post.JOB.log \
#	 post-to-phone-post $dir/$test/final.mdl \
#	 "ark:gunzip -c $dir/$test/post.JOB.gz|" "ark,t:|gzip -c > $dir/$test/post-phone.JOB.gz" 
    $cmd JOB=1:$nj $dir/$test/log/get-post-on-ali.JOB.log \
     get-post-on-ali "ark:gunzip -c $dir/$test/post-phone.JOB.gz|" "ark:gunzip -c $dir/$test/ali-phone.JOB.gz|" ark,t:$dir/$test/phones.JOB.conf
done
