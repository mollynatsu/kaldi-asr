#!/bin/bash

data_root=/asr/librispeech/data

. ./cmd.sh || exit 1
. ./path.sh || exit 1

nj=4 # number of parallel jobs

# echo
# echo "===== PREPARING DATA ====="
# echo
# 
# # format the data as Kaldi data directories
# for part in dev-clean test-clean train-clean-100; do
#     local/data_prep.sh $data_root/LibriSpeech/$part data/$(echo $part | sed s/-/_/g)
# done
# 
# echo
# echo "===== LANGUAGE MODEL CREATION ====="
# echo
# 
# local/prepare_dict.sh --stage 3 --nj $nj --cmd "$train_cmd" \
#  data/local/lm data/local/lm data/local/dict_nosp
# 
# utils/prepare_lang.sh data/local/dict_nosp \
#  "<UNK>" data/local/lang_tmp_nosp data/lang_nosp
# 
# local/format_lms.sh --src-dir data/lang_nosp data/local/lm
# 
# # Create ConstArpaLm format language model for full 3-gram and 4-gram LMs
# utils/build_const_arpa_lm.sh data/local/lm/lm_tglarge.arpa.gz \
#  data/lang_nosp data/lang_nosp_test_tglarge
# utils/build_const_arpa_lm.sh data/local/lm/lm_fglarge.arpa.gz \
#  data/lang_nosp data/lang_nosp_test_fglarge
# 
# echo
# echo "===== FEATURES EXTRACTION ====="
# echo
# 
# mfccdir=mfcc
# for part in dev_clean test_clean train_clean_100; do
#     steps/make_mfcc.sh --cmd "$train_cmd" --nj $nj data/$part exp/make_mfcc/$part $mfccdir
#     steps/compute_cmvn_stats.sh data/$part exp/make_mfcc/$part $mfccdir
# done
# 
# utils/subset_data_dir.sh --shortest data/train_clean_100 2000 data/train_2kshort
# 
# echo
# echo "===== MONO TRAINING ====="
# echo
# 
# # train a monophone system
# steps/train_mono.sh --boost-silence 1.25 --nj $nj --cmd "$train_cmd" \
#  data/train_2kshort data/lang_nosp exp/mono  || exit 1

# decode using the monophone model
utils/mkgraph.sh data/lang_nosp_test_tgsmall \
    exp/mono exp/mono/graph_nosp_tgsmall
for test in test_clean dev_clean; do
    steps/decode.sh --nj $nj  --cmd "$decode_cmd" exp/mono/graph_nosp_tgsmall \
     data/$test exp/mono/decode_nosp_tgsmall_$test
done



